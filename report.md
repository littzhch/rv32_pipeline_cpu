# 计算机组成原理 实验报告

- 实验题目：综合实验
- 学生姓名：张驰
- 学生学号：PB20111627

## 实验内容

### 支持 RV32I 全部非特权指令

#### 支持的非特权指令

```
            funct7      funct3      opcode
R type instructions
add         0           000         0110011
sub         0100000     000         0110011

and         0           111         0110011
or          0           110         0110011
xor         0           100         0110011

sll         0           001         0110011
srl         0           101         0110011
sra         0100000     101         0110011

slt         0           010         0110011
sltu        0           011         0110011


B type instructions
beq                     000         1100011
bge                     101         1100011
bgeu                    111         1100011
blt                     100         1100011
bltu                    110         1100011
bne                     001         1100011


I type instructions (unpriviliged)
addi                    000         0010011

andi                    111         0010011
ori                     110         0010011
xori                    100         0010011

slli        0           001         0010011
srli        0           101         0010011
srai        0100000     101         0010011

slti                    010         0010011
sltiu                   011         0010011


Load instructions (I type)
lb                      000         0000011
lbu                     100         0000011
lh                      001         0000011
lhu                     101         0000011
lw                      010         0000011


Store instructions(S type)
sb                      000         0100011
sh                      001         0100011
sw                      010         0100011


U type instructions
auipc                               0010111
lui                                 0110111


jal                                 1101111
jalr                    010         1100111
```

#### 控制模块代码

```verilog
module control(
    input [4:0] opcode,
    input [2:0] funct3,
    input [6:0] funct7,
    input if_exception,
    input [3:0] if_exception_cause,

    output reg reg_enable,
    output reg mem_enable,
    output reg [2:0] imm_type,
    output reg [3:0] alu_mode,
    output reg branch_enable,
    output reg jump,

    output reg alu_src1_sel,
    output reg alu_src2_sel,
    output reg reg_data_sel,
    output reg alu_mem_sel,
    output reg pc_rs1_sel,
    output reg busdata_rs2_rs1_sel,
    output reg [2:0] memdata_ctl,

    output reg exception,
    output reg [3:0] exception_cause
    // memdata_ctl[1:0]:
    // 00: byte
    // 01: half word
    // 10: word
    //
    // memdata_ctl[2]:
    // 0: sext
    // 1: zext
);

    always @ (*) begin
        reg_enable = 0;
        mem_enable = 0;
        imm_type = 0;
        alu_mode = {1'b0, funct3};
        branch_enable = 0;
        jump = 0;
        alu_src1_sel = 0;
        alu_src2_sel = 0;
        reg_data_sel = 0;
        alu_mem_sel = 0;
        pc_rs1_sel = 0;
        busdata_rs2_rs1_sel = 0;
        memdata_ctl = funct3;
        exception = if_exception;
        exception_cause = if_exception_cause;
        if (! exception) begin
            if (opcode == 5'b01100) begin  // R type
                alu_mode[3] = funct7[5];
                reg_enable = 1;
            end
            else if (opcode == 5'b11000) begin  // B type
                imm_type = 3'b010;
                branch_enable = 1;
            end
            else if (opcode == 5'b00100) begin  // I type
                if (funct3 == 3'b101)
                    alu_mode[3] = funct7[5];
                reg_enable = 1;
                imm_type = 3'b000;
                alu_src2_sel = 1;
            end
            else if (opcode == 5'b00000) begin  // load
                alu_mode = 0;
                reg_enable = 1;
                imm_type = 3'b000;
                alu_src2_sel = 1;
                alu_mem_sel = 1;
            end
            else if (opcode == 5'b01000) begin  // save
                alu_mode = 0;
                mem_enable = 1;
                imm_type = 3'b001;
                alu_src2_sel = 1;
            end
            else if (opcode == 5'b00101) begin  // (auipc)
                alu_mode = 0;
                reg_enable = 1;
                imm_type = 3'b011;
                alu_src1_sel = 1;
                alu_src2_sel = 1;
            end
            else if (opcode == 5'b01101) begin // (lui)
                alu_mode = 4'b1111;
                reg_enable = 1;
                imm_type = 3'b011;
                alu_src2_sel = 1;
            end
            else if (opcode == 5'b11011) begin  // (jal)
                reg_enable = 1;
                imm_type = 3'b100;
                jump = 1;
                reg_data_sel = 1;
            end
            else if (opcode == 5'b11001) begin  // (jalr)
                reg_enable = 1;
                imm_type = 3'b000;
                jump = 1;
                reg_data_sel = 1;
                pc_rs1_sel = 1;
            end
            else if (opcode == 5'b11100) begin // system
                if (funct3 == 0) begin
                    exception = 1;
                    if (funct7 == 0) // ecall
                        exception_cause = 11;
                    else if (funct7 == 7'b0011000) // mret
                        exception_cause = 10;
                    else
                        exception_cause = 2;
                end
                else begin
                    alu_mode = 4'b1111;
                    imm_type = 3'b101;
                    busdata_rs2_rs1_sel = 1;
                    mem_enable = 1;
                    alu_mem_sel = 1;
                    reg_enable = 1;
                    alu_src2_sel = 1;
                end
            end
            else begin
                exception = 1;
                exception_cause = 2;
            end
        end
    end
endmodule
```

### 添加 UART 串口读写外设

#### 总线接口

UART串口设备控制器(`src/design/uart.v: uart模块`)通过5个32位寄存器与CPU交互，这些寄存器被映射到`0xffff_ffec`到`0xffff_fffc`的总线地址上。

UART读取模块(`src/design/uart.v: uart_read模块`)占用三个寄存器：`uart_read_next`, `uart_read_data`, `uart_read_ready`。其中，`uart_read_next`为只写寄存器，剩余两个为只读寄存器。`uart_read_ready`寄存器用来指示UART读取模块状态：当该寄存器值为1时，`uart_read_data`寄存器中的低八位数据为有效的待读数据，且UART读取模块此时处于空闲状态；当该寄存器值为0时，UART读取模块处于工作状态，不响应CPU请求，此时`uart_read_data`中的数据有效性没有保证。`uart_read_next`寄存器用来向UART读取模块发送读取下一个字节的指令，当CPU向该寄存器发出写指令时，UART读取模块将`uart_read_ready`寄存器置为0并开始工作，在成功刷新`uart_read_data`寄存器中的数据为下一个待读字节后，UART读取模块将`uart_read_ready`寄存器重新置为1并进入空闲状态。如果串口一直没有读取到下一个字节的数据，UART读取模块将持续处于工作状态，即`uart_read_ready`持续为0。

UART发送模块(`src/design/uart.v: uart_send模块`)占用两个寄存器：`uart_send_data`(只写) 和`uart_send_ready`(只读)。`uart_send_ready`寄存器用来指示UART发送模块的状态：当该寄存器值为1时，模块处于空间状态；当该寄存器的值为0时，模块正在发送数据，不响应CPU请求。当CPU向`uart_send_data`写入数据时，UART发送模块将从空闲状态转为工作状态，发送`uart_send_data`寄存器的低8位数据，发送完成后，模块将回到空闲状态。

#### 实现

UART读取模块使用一个循环队列作为输入缓冲区。从串口读取的数据将进入队列，`uart_read_data`指向队头字节，`uart_read_next`的写操作将导致队头元素出队。

### 支持部分特权指令和`CSR`，支持异常和中断

#### 支持的指令

```
csrrw
csrrc
csrrs
ecall
mret
```

#### 支持的`CSR`

```
mstatus
mie
mtvec
mscratch
mepc
mcause
mtval
mip
```

#### 实现

`CSR` 在 `trap_csr_control` 模块中实现，并被映射到总线地址`0xfffe_0xxx`处，从而支持`csr`指令。


## 总结与思考

### 难易程度

较难

### 任务量

较大
