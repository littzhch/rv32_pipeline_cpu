	.file	"syscall.c"
	.option nopic
	.text
	.align	2
	.globl	ecall_handler
	.type	ecall_handler, @function
	j main
ecall_handler:
	addi	sp,sp,-32
	sw	s0,28(sp)
	addi	s0,sp,32
	sw	a0,-20(s0)
	sw	a1,-24(s0)
	lw	a4,-24(s0)
	li	a5,1
	bne	a4,a5,.L6
	nop
.L3:
	li	a5,-4
	lw	a5,0(a5)
	beqz	a5,.L3
	li	a5,-8
	lw	a5,0(a5)
	sw	a5,-20(s0)
	li	a5,-12
	li	a4,1
	sw	a4,0(a5)
	lw	a5,-20(s0)
	j	.L4
.L6:
	nop
.L5:
	li	a5,-16
	lw	a5,0(a5)
	beqz	a5,.L5
	li	a5,-20
	lw	a4,-20(s0)
	sw	a4,0(a5)
	li	a5,0
.L4:
	mv	a0,a5
	lw	s0,28(sp)
	addi	sp,sp,32
	jr	ra
	.size	ecall_handler, .-ecall_handler
	.ident	"GCC: (GNU MCU Eclipse RISC-V Embedded GCC, 64-bit) 8.2.0"

main:
nop