.globl _start

_start:
li sp, 2000
la t1, ecall_handler
csrw mtvec, t1
li t1, 2000
csrw mscratch, t1
j main


ecall_handler:
    beqz a7, send
        loop1:
        lw a1, -4(zero)
        beqz a1, loop1
        lw a0, -8(zero)
        sw a2, -12(zero)
        mret
    send:
        loop2:
        lw a1, -16(zero)
        beqz a1, loop2
        sw a0, -20(zero)
    mret


main:
    li a7, 1
    ecall
    addi a0, a0, 1
    li a7, 0
    ecall
    j main





