#define uart_read_ready (*(int*)(-4))
#define uart_read_data  (*(int*)(-8))
#define uart_read_next  (*(int*)(-12))
#define uart_send_ready (*(int*)(-16))
#define uart_send_data  (*(int*)(-20))


__attribute__((fastcall)) int ecall_handler(int ch, int type) {
    if (type == 1) { // read
        while (uart_read_ready == 0);
        ch = uart_read_data;
        uart_read_next = 1;
        return ch;
    } else {
        while (uart_send_ready == 0);
        uart_send_data = ch;
        return 0;
    }
}

int uart_read(void);
void uart_send(int);

void main(void) {
    while (1) {
        uart_send(uart_read() + 1);
    }
}
