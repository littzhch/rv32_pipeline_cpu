	.file	"uart_test.c"
	.option nopic
	.text
	.align	2
	.globl	putchar
	.type	putchar, @function

li sp, 1000
j main
putchar:
	addi	sp,sp,-32
	sw	s0,28(sp)
	addi	s0,sp,32
	sw	a0,-20(s0)
	nop
.L2:
	li	a5,-16
	lw	a5,0(a5)
	beqz	a5,.L2
	li	a5,-20
	lw	a4,-20(s0)
	sw	a4,0(a5)
	nop
	lw	s0,28(sp)
	addi	sp,sp,32
	jr	ra
	.size	putchar, .-putchar
	.align	2
	.globl	getchar
	.type	getchar, @function
getchar:
	addi	sp,sp,-32
	sw	s0,28(sp)
	addi	s0,sp,32
	nop
.L4:
	li	a5,-4
	lw	a5,0(a5)
	beqz	a5,.L4
	li	a5,-8
	lw	a5,0(a5)
	sw	a5,-20(s0)
	li	a5,-12
	li	a4,1
	sw	a4,0(a5)
	lw	a5,-20(s0)
	mv	a0,a5
	lw	s0,28(sp)
	addi	sp,sp,32
	jr	ra
	.size	getchar, .-getchar
	.align	2
	.globl	main
	.type	main, @function
main:
	addi	sp,sp,-32
	sw	ra,28(sp)
	sw	s0,24(sp)
	addi	s0,sp,32
.L7:
	call	getchar
	sw	a0,-20(s0)
	lw	a5,-20(s0)
	addi	a5,a5,1
	mv	a0,a5
	call	putchar
	j	.L7
	.size	main, .-main
	.ident	"GCC: (GNU MCU Eclipse RISC-V Embedded GCC, 64-bit) 8.2.0"
