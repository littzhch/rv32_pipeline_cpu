#define uart_read_ready (*(int*)(-4))
#define uart_read_data  (*(int*)(-8))
#define uart_read_next  (*(int*)(-12))
#define uart_send_ready (*(int*)(-16))
#define uart_send_data  (*(int*)(-20))


void putchar(int ch) {
    while (uart_send_ready == 0);
    uart_send_data = ch;
}

int getchar(void) {
    int ch;
    while (uart_read_ready == 0);
    ch = uart_read_data;
    uart_read_next = 1;
    return ch;
}

void main(void) {
    int ch;
    while (1) {
        ch = getchar();
        putchar(ch + 1);
    }
}
