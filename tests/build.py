import os
import sys

prefix = "riscv-none-embed-"


def system(cmd):
    print(cmd)
    os.system(cmd)


def little2big(little: str) -> str:
    return little[6:] + little[4:6] + little[2:4] + little[0:2]


def compile_src(source_name: str) -> str:
    if source_name.endswith(".s"):
        return source_name
    elif source_name.endswith(".c"):
        raw_name = source_name[:-2]
        system(f"{prefix}gcc -S -march=rv32i -mabi=ilp32 {source_name}")
        return f"{raw_name}.s"
    else:
        print("unknown compile source")
        exit(-1)


def modify_section(source_name: str) -> str:
    if not source_name.endswith(".s"):
        print("unknown modify source")
        exit(-1)

    raw_name = source_name[:-2]
    fr = open(source_name, "r")
    fw = open(f"{raw_name}_mod.s", "w")
    fw.write(fr.read().replace(".rodata", ".data").replace(".sdata", ".data"))
    fr.close()
    fw.close()

    return f"{raw_name}_mod.s"


def assemble(source_name: str) -> str:
    if not source_name.endswith(".s"):
        print("unknown assemble source")
        exit(-1)

    raw_name = source_name[:-2]
    system(f"{prefix}as -march=rv32i {source_name} -o {raw_name}.o")

    return f"{raw_name}.o"


def link(source_list: list[str], text_addr: str, data_addr: str) -> str:
    source_string = " "
    for src in source_list:
        source_string += f"{src} "

    system(f"{prefix}ld -Ttext {text_addr} -Tdata {data_addr} --no-relax {source_string} -oa.out")
    return "a.out"


def objdump(binary_name: str) -> (str, str):
    text = ""
    data = ""

    cmd = os.popen(f"{prefix}objdump -s a.out")
    raw_data = cmd.read()

    raw_data = raw_data.split("Contents of section .")
    for s in raw_data:
        if s.startswith("text:"):
            lines = s.split("\n")[1:]
            for line in lines:
                sublines = (line[6:].split(" "))[:4]
                for subline in sublines:
                    if len(subline) == 8:
                        text += little2big(subline)
                        text += '\n'
                    elif len(subline) < 8 and len(subline) > 0:
                        subline += '0' * (8 - len(subline))
                        text += little2big(subline)
                        text += '\n'
        elif s.startswith("data:"):
            lines = s.split("\n")[1:]
            for line in lines:
                sublines = (line[6:].split(" "))[:4]
                for subline in sublines:
                    if len(subline) == 8:
                        data += little2big(subline)
                        data += '\n'
                    elif len(subline) < 8 and len(subline) > 0:
                        subline += '0' * (8 - len(subline))
                        data += little2big(subline)
                        data += '\n'

    return text, data


def dump_result(text: str, data: str, data_addr=None):
    if data_addr is None:
        f = open("../text.txt", "w")
        f.write(text)
        f.close()
        f = open("../data.txt", "w")
        f.write(data)
        f.close()
    else:
        f = open("../binary.txt", "w")
        f.write(text)
        num = int(data_addr.replace("0x", ""), base=16)
        num -= len(text.split("\n")) * 4
        num += 4
        f.write("00000000\n" * int((num / 4)))
        f.write(data)
        f.close()


def main():
    print(sys.argv)
    source_list = []
    text_addr = None
    data_addr = None
    output_type = 0

    for arg in sys.argv[1:]:
        if arg[0] != '-':
            source_list.append(arg)
        else:
            if arg.startswith("-text="):
                text_addr = arg.replace("-text=", "")
            elif arg.startswith("-data="):
                data_addr = arg.replace("-data=", "")
            elif arg.startswith("-type"):
                output_type = int(arg.replace("-type", ""))
            else:
                print(f"{arg} ignored")

    source_string = " "
    for src in source_list:
        source_string += f"{src} "

    os.chdir(os.path.dirname(os.path.abspath(source_list[0])))
    system("mkdir build")
    for src in source_list:
        system(f"powershell cp {src} ./build")
    os.chdir("./build")

    compiled_list = []
    for src in source_list:
        compiled_list.append(compile_src(src))

    modified_list = []
    for src in compiled_list:
        modified_list.append(modify_section(src))

    assembled_list = []
    for src in modified_list:
        assembled_list.append(assemble(src))

    linked_bin = link(assembled_list, text_addr, data_addr)

    text, data = objdump(linked_bin)
    print(text)
    print(data)

    dump_result(text, data, data_addr if output_type == 1 else None)


if __name__ == "__main__":
    main()





