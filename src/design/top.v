module top (
    input clk,
    input rst,

    output uart_rx,
    input uart_tx
);
    wire [31:0] bus_addr;
    wire [31:0] bus_wdata;
    wire bus_we;
    wire [31:0] bus_rdata;

    uart i_uart (
        clk, rst,
        bus_addr,
        bus_wdata,
        bus_we,
        bus_rdata,
        uart_rx,
        uart_tx
    );

    cpu i_cpu (
        clk, rst,
        bus_addr,
        bus_wdata,
        bus_we,
        bus_rdata
    );
endmodule