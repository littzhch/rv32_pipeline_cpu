module program_counter(
    input clk,
    input rst,
    input stall,
    input branch,
    input [31:0] pc_new,
    
    output reg [31:0] pc
);
    always @ (posedge clk, posedge rst) begin
        if (rst)
            pc <= 0;
        else if (branch)  // 确保branch的优先级高于stall
            pc <= pc_new;
        else if (! stall)
            pc <= pc + 4;
    end
endmodule