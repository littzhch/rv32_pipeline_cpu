module instr_decoder(
    input [31:0] instr,

    output [4:0] opcode,
    output [4:0] rd,
    output [4:0] rs1,
    output [4:0] rs2,
    output [2:0] funct3,
    output [6:0] funct7,
    output [24:0] immsrc
);
    assign opcode = instr[6:2];
    assign rd = instr[11:7];
    assign rs1 = instr[19:15];
    assign rs2 = instr[24:20];
    assign funct3 = instr[14:12];
    assign funct7 = instr[31:25];
    assign immsrc = instr[31:7];
endmodule