module control(
    input [4:0] opcode,
    input [2:0] funct3,
    input [6:0] funct7,
    input if_exception,
    input [3:0] if_exception_cause,

    output reg reg_enable,
    output reg mem_enable,
    output reg [2:0] imm_type,
    output reg [3:0] alu_mode,
    output reg branch_enable,
    output reg jump,

    output reg alu_src1_sel,
    output reg alu_src2_sel,
    output reg reg_data_sel,
    output reg alu_mem_sel,
    output reg pc_rs1_sel,
    output reg busdata_rs2_rs1_sel,
    output reg [2:0] memdata_ctl,

    output reg exception,
    output reg [3:0] exception_cause
    // memdata_ctl[1:0]:
    // 00: byte
    // 01: half word
    // 10: word
    //
    // memdata_ctl[2]:
    // 0: sext
    // 1: zext
);

    always @ (*) begin
        reg_enable = 0;
        mem_enable = 0;
        imm_type = 0;
        alu_mode = {1'b0, funct3};
        branch_enable = 0;
        jump = 0;
        alu_src1_sel = 0;
        alu_src2_sel = 0;
        reg_data_sel = 0;
        alu_mem_sel = 0;
        pc_rs1_sel = 0;
        busdata_rs2_rs1_sel = 0;
        memdata_ctl = funct3;
        exception = if_exception;
        exception_cause = if_exception_cause;
        if (! exception) begin
            if (opcode == 5'b01100) begin  // R type
                alu_mode[3] = funct7[5];
                reg_enable = 1;
            end
            else if (opcode == 5'b11000) begin  // B type
                imm_type = 3'b010;
                branch_enable = 1;
            end
            else if (opcode == 5'b00100) begin  // I type
                if (funct3 == 3'b101)
                    alu_mode[3] = funct7[5];
                reg_enable = 1;
                imm_type = 3'b000;
                alu_src2_sel = 1;
            end
            else if (opcode == 5'b00000) begin  // load
                alu_mode = 0;
                reg_enable = 1;
                imm_type = 3'b000;
                alu_src2_sel = 1;
                alu_mem_sel = 1;
            end
            else if (opcode == 5'b01000) begin  // save
                alu_mode = 0;
                mem_enable = 1;
                imm_type = 3'b001;
                alu_src2_sel = 1;
            end
            else if (opcode == 5'b00101) begin  // (auipc)
                alu_mode = 0;
                reg_enable = 1;
                imm_type = 3'b011;
                alu_src1_sel = 1;
                alu_src2_sel = 1;
            end
            else if (opcode == 5'b01101) begin // (lui)
                alu_mode = 4'b1111;
                reg_enable = 1;
                imm_type = 3'b011;
                alu_src2_sel = 1;
            end
            else if (opcode == 5'b11011) begin  // (jal)
                reg_enable = 1;
                imm_type = 3'b100;
                jump = 1;
                reg_data_sel = 1;
            end
            else if (opcode == 5'b11001) begin  // (jalr)
                reg_enable = 1;
                imm_type = 3'b000;
                jump = 1;
                reg_data_sel = 1;
                pc_rs1_sel = 1;
            end
            else if (opcode == 5'b11100) begin // system
                if (funct3 == 0) begin
                    exception = 1;
                    if (funct7 == 0) // ecall
                        exception_cause = 11;
                    else if (funct7 == 7'b0011000) // mret
                        exception_cause = 10;
                    else
                        exception_cause = 2;
                end
                else begin
                    alu_mode = 4'b1111;
                    imm_type = 3'b101;
                    busdata_rs2_rs1_sel = 1;
                    mem_enable = 1;
                    alu_mem_sel = 1;
                    reg_enable = 1;
                    alu_src2_sel = 1;
                end
            end
            else begin
                exception = 1;
                exception_cause = 2;
            end
        end
    end
endmodule
