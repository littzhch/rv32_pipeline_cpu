module internal_bus_controller #(
    io_base = 32'hffff_ffec,
    csr_base = 32'hfffe_0000,
    mem_base = 32'h0
    ) (
    input [31:0] bus_addr,
    input [31:0] io_rdata,
    input [31:0] csr_rdata,
    input [31:0] mem_rdata,
    output reg [31:0] rdata
);
    always @ (*) begin
        rdata = 0;
        if (bus_addr >= io_base)
            rdata = io_rdata;
        else if (bus_addr >= csr_base)
            rdata = csr_rdata;
        else if (bus_addr >= mem_base)
            rdata = mem_rdata;
    end
endmodule