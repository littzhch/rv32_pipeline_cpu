module data_hazard (
    input clk,
    input [4:0] rs1,
    input [4:0] rs2,
    input [4:0] ex_rd,
    input [4:0] mem_rd,
    input ex_reg_we,
    input mem_reg_we,
    input ex_reg_data_sel,
    input ex_alu_mem_sel,

    output reg loaduse_happen,
    output reg [1:0] forwarding_rs1_sel,
    output reg [1:0] forwarding_rs2_sel
);
    wire ex_load;
    wire rs1_loaduse_happen;
    wire rs2_loaduse_happen;
    assign ex_load = ex_reg_data_sel == 0 && ex_alu_mem_sel == 1;
    assign rs1_loaduse_happen = ex_load && ex_rd != 0 && ex_rd == rs1;
    assign rs2_loaduse_happen = ex_load && ex_rd != 0 && ex_rd == rs2;
    
    always @ (*) begin
        if (rs1_loaduse_happen || rs2_loaduse_happen) begin
            loaduse_happen <= 1;
        end else begin
            loaduse_happen <= 0;
        end
    end

    always @ (*) begin
        if (rs1_loaduse_happen) begin
            forwarding_rs1_sel = 2;
            forwarding_rs2_sel = 0;
        end 
        else if (rs2_loaduse_happen) begin
            forwarding_rs1_sel = 0;
            forwarding_rs2_sel = 2;         
        end
        else begin
            forwarding_rs1_sel = 0;
            forwarding_rs2_sel = 0;
            if (rs1) begin
                if (mem_reg_we && mem_rd == rs1)
                    forwarding_rs1_sel = 2;
                if (ex_reg_we && ex_rd == rs1)
                    forwarding_rs1_sel = 1;
            end
            if (rs2) begin
                if (mem_reg_we && mem_rd == rs2)
                    forwarding_rs2_sel = 2;
                if (ex_reg_we && ex_rd == rs2)
                    forwarding_rs2_sel = 1;
            end
        end
    end
endmodule