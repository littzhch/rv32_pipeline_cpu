module data_mem (
    input clk,
    input we,
    input [2:0] memdata_ctl,
    input [31:0] addr,
    input [31:0] wdata,
    output reg [31:0] rdata,

    input [31:0] instr_addr,
    output [31:0] instr_data
);
    reg [31:0] mem_wdata;
    wire [31:0] mem_rdata;

    always @ (*) begin
        if (memdata_ctl[1:0] == 0) begin
            mem_wdata = {mem_rdata[31:8], wdata[7:0]};
            rdata = {{24{mem_rdata[7] & ~memdata_ctl[2]}}, mem_rdata[7:0]};
        end
        else if (memdata_ctl[1:0] == 1) begin
            mem_wdata = {mem_rdata[31:16], wdata[15:0]};
            rdata = {{16{mem_rdata[15] & ~memdata_ctl[2]}}, mem_rdata[15:0]};
        end
        else begin
            mem_wdata = wdata;
            rdata = mem_rdata;
        end
    end
    
    dist_mem_gen_data data_mem_inst (
        .a(addr >> 2),
        .d(mem_wdata),
        .spo(mem_rdata),
        .clk(clk),
        .we(we),
        .dpra(instr_addr >> 2),
        .dpo(instr_data)
    );
    
endmodule