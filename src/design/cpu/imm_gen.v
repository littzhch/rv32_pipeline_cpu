module imm_gen #(csr_base = 32'hfffe_0000) (
    input [31:7] instr,
    input [2:0] imm_type,
    // 000  I型：SEXT
    // 001  S型：SEXT
    // 010  B型：SEXT，第0位置0
    // 011  U型：SEXT，低位置0
    // 100 UJ型；SEXT，第0位置0
    // 101 生成CSR内存映射地址
    output reg [31:0] imm
);
    always @ (*) begin
        imm <= 0;
        case (imm_type)
        3'b000: begin
            imm[11:0] <= instr[31:20];
            imm[31:12] <= {20{instr[31]}};
        end
        3'b001: begin
            imm[11:5] <= instr[31:25];
            imm[4:0] <= instr[11:7];
            imm[31:12] <= {20{instr[31]}};
        end
        3'b010: begin
            imm[0:0] <= 0;
            {imm[4:1], imm[11:11]} <= instr[11:7];
            {imm[12:12], imm[10:5]} <= instr[31:25];
            imm[31:13] <= {19{instr[31]}};
        end
        3'b011: begin
            imm[31:12] <= instr[31:12];
            imm[11:0] <= 0;
        end
        3'b100: begin
            {imm[20:20], imm[10:1], imm[11:11], imm[19:12]} <= instr[31:12];
            imm[0:0] <= 0;
            imm[31:21] <= {11{instr[31]}};
        end
        3'b101: begin
            imm <= {csr_base[31:12], instr[31:20]};
        end
        endcase
    end
endmodule