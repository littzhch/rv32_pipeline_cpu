module mux2 #(WIDTH = 32) (
    input [WIDTH:1] data0,
    input [WIDTH:1] data1,
    input sel,

    output [WIDTH:1] dout
);
    assign dout = sel ? data1 : data0;
endmodule

module mux3 #(WIDTH = 32) (
    input [WIDTH:1] data0,
    input [WIDTH:1] data1,
    input [WIDTH:1] data2,
    input [1:0] sel,

    output reg [WIDTH:1] dout
);
    always @ (*) begin
        case (sel)
            0: dout = data0;
            1: dout = data1;
            2: dout = data2;
            default: dout = data0;
        endcase
    end
endmodule