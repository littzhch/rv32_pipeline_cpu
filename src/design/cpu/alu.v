module alu (
    input [31:0] number1,
    input [31:0] number2,
    input [3:0] mode,
    // mode:
    // 0000: add  beq
    // 1000: sub  beq
    // 0111: and  bgeu
    // 0110: or   bltu
    // 0100: xor  blt
    // 0001: sll  bne
    // 0101: srl  bge
    // 1101: sra  bge
    // 0010: slt  -
    // 0011: sltu -
    // 1111: <reuslt=number2>

    output reg [31:0] result,
    output reg branch
);
    always @ (*) begin
        case (mode[2:0])
            3'b000: branch = number1 == number2;
            3'b111: branch = number1 >= number2;
            3'b110: branch = number1 < number2;
            3'b100: branch = $signed(number1) < $signed(number2);
            3'b001: branch = number1 != number2;
            3'b101: branch = $signed(number1) >= $signed(number2);
            default: branch = 0;
        endcase
    end

    always @ (*) begin
        case (mode)
            4'b0000: result = number1 + number2;
            4'b1000: result = number1 - number2;
            4'b0111: result = number1 & number2;
            4'b0110: result = number1 | number2;
            4'b0100: result = number1 ^ number2;
            4'b0001: result = number1 << number2[4:0];
            4'b0101: result = number1 >> number2[4:0];
            4'b1101: result = number1 >>> number2[4:0];
            4'b0010: result = $signed(number1) < $signed(number2);
            4'b0011: result = number1 < number2;
            4'b1111: result = number2;
            default: result = 0;
        endcase
    end
endmodule
