module cpu (
    input clk,
    input rst,

    // bus inerface
    output [31:0] bus_addr,
    output [31:0] bus_wdata,
    output bus_we,
    input [31:0] bus_rdata
);
    wire [2:0] bus_ctl;

	wire [31:0] i_data_mem_rdata;
    wire [31:0] i_internal_bus_controller_rdata;
	wire [31:0] i_instr_mem_data;
	wire [31:0] i_alu_result;
	wire i_alu_branch;
	wire i_control_reg_enable;
	wire i_control_mem_enable;
	wire [2:0] i_control_imm_type;
	wire [3:0] i_control_alu_mode;
	wire i_control_branch_enable;
	wire i_control_jump;
	wire i_control_alu_src1_sel;
	wire i_control_alu_src2_sel;
	wire i_control_reg_data_sel;
	wire i_control_alu_mem_sel;
    wire i_control_pc_rs1_sel;
    wire i_control_busdata_rs2_rs1_sel;
    wire [2:0] i_control_memdata_ctl;
    wire i_control_exception;
    wire [3:0] i_control_exception_cause;
	wire [31:0] i_imm_gen_imm;
	wire [4:0] i_instr_decoder_opcode;
	wire [4:0] i_instr_decoder_rd;
	wire [4:0] i_instr_decoder_rs1;
	wire [4:0] i_instr_decoder_rs2;
	wire [2:0] i_instr_decoder_funct3;
	wire [6:0] i_instr_decoder_funct7;
	wire [24:0] i_instr_decoder_immsrc;
	wire [31:0] i_register_file_rdata1;
	wire [31:0] i_register_file_rdata2;
	wire [31:0] i_register_file_rdatadbg;
	wire [31:0] i_program_counter_pc;

    wire [31:0] alu_src1_mux_dout;
    wire [31:0] alu_src2_mux_dout;
    wire [31:0] alu_mem_mux_dout;
    wire [31:0] reg_data_mux_dout;
    wire [32:1] rs1_forwarding_mux_dout;
	wire [32:1] rs2_forwarding_mux_dout;
    wire [31:0] pc_rs1_mux_dout;
    wire [31:0] busdata_rs2_rs1_mux_dout;

    wire [1:0] i_data_hazard_forwarding_rs1_sel;
	wire [1:0] i_data_hazard_forwarding_rs2_sel;
    wire i_data_hazard_loaduse_happen;
    wire branch_result;

    wire i_trap_csr_control_jump;
	wire [31:0] i_trap_csr_control_target_pc;
	wire [31:0] i_trap_csr_control_bus_rdata;

    wire if_exception;
    wire [3:0] if_exception_cause;


    wire [31:0] id_i_program_counter_pc;
    wire [31:0] id_i_instr_mem_data;
    wire id_exception;
    wire [3:0] id_exception_cause;

    wire [31:0] ex_i_program_counter_pc;
    wire [31:0] ex_i_instr_mem_data;
    wire ex_exception;
    wire [3:0] ex_exception_cause;
    wire [4:0] ex_i_instr_decoder_rd;
    wire [31:0] ex_i_register_file_rdata1;
    wire [31:0] ex_i_register_file_rdata2;
    wire [31:0] ex_i_imm_gen_imm;
    wire [1:0] ex_i_data_hazard_forwarding_rs1_sel;
    wire [1:0] ex_i_data_hazard_forwarding_rs2_sel;
    wire ex_i_control_reg_enable;
    wire ex_i_control_mem_enable;
    wire [3:0] ex_i_control_alu_mode;
    wire ex_i_control_branch_enable;
    wire ex_i_control_jump;
    wire ex_i_control_alu_src1_sel;
    wire ex_i_control_alu_src2_sel;
    wire ex_i_control_reg_data_sel;
    wire ex_i_control_alu_mem_sel;
    wire ex_i_control_pc_rs1_sel;
    wire ex_i_control_busdata_rs2_rs1_sel;
    wire [2:0] ex_i_control_memdata_ctl;

    wire [31:0] mem_i_program_counter_pc;
    wire [31:0] mem_i_instr_mem_data;
    wire mem_exception;
    wire [3:0] mem_exception_cause;
    wire [4:0] mem_i_instr_decoder_rd;
    wire mem_i_alu_branch;
    wire [31:0] mem_i_alu_result;
    wire [31:0] mem_i_busdata_rs2_rs1_mux_dout;
    wire [31:0] mem_i_pc_imm_sum;
    wire mem_i_control_reg_enable;
    wire mem_i_control_mem_enable;
    wire mem_i_control_branch_enable;
    wire mem_i_control_jump;
    wire mem_i_control_reg_data_sel;
    wire mem_i_control_alu_mem_sel;
    wire [2:0] mem_i_control_memdata_ctl;

    wire [31:0] wb_i_pc_add_4;
    wire [4:0] wb_i_instr_decoder_rd;
    wire [31:0] wb_alu_mem_mux_dout;
    wire wb_i_control_reg_enable;
    wire wb_i_control_reg_data_sel;


    pipeline_register #(.WIDTH(32+32+1+4)) if_id (
        .clk(clk),
        .rst(rst),
        .flush(branch_result),
        .stall(i_data_hazard_loaduse_happen),
        .signal_in(
            {
                i_program_counter_pc,
                i_instr_mem_data,
                if_exception,
                if_exception_cause
            }
        ),
        .signal_out(
            {
                id_i_program_counter_pc,
                id_i_instr_mem_data,
                id_exception,
                id_exception_cause
            }
        )
    );

    pipeline_register #(.WIDTH(32+32+1+4+5+32+32+32+2+2+1+1+4+1+1+1+1+1+1+1+1+3)) id_ex (
        .clk(clk),
        .rst(rst),
        .flush(branch_result || i_data_hazard_loaduse_happen),
        .stall(1'b0),
        .signal_in(
            {
                id_i_program_counter_pc,
                id_i_instr_mem_data,
                i_control_exception,
                i_control_exception_cause,
                i_instr_decoder_rd,
                i_register_file_rdata1,
                i_register_file_rdata2,
                i_imm_gen_imm,
                i_data_hazard_forwarding_rs1_sel,
                i_data_hazard_forwarding_rs2_sel,
                i_control_reg_enable,
                i_control_mem_enable,
                i_control_alu_mode,
                i_control_branch_enable,
                i_control_jump,
                i_control_alu_src1_sel,
                i_control_alu_src2_sel,
                i_control_reg_data_sel,
                i_control_alu_mem_sel,
                i_control_pc_rs1_sel,
                i_control_busdata_rs2_rs1_sel,
                i_control_memdata_ctl
            }
        ),
        .signal_out(
            {
                ex_i_program_counter_pc,
                ex_i_instr_mem_data,
                ex_exception,
                ex_exception_cause,
                ex_i_instr_decoder_rd,
                ex_i_register_file_rdata1,
                ex_i_register_file_rdata2,
                ex_i_imm_gen_imm,
                ex_i_data_hazard_forwarding_rs1_sel,
                ex_i_data_hazard_forwarding_rs2_sel,
                ex_i_control_reg_enable,
                ex_i_control_mem_enable,
                ex_i_control_alu_mode,
                ex_i_control_branch_enable,
                ex_i_control_jump,
                ex_i_control_alu_src1_sel,
                ex_i_control_alu_src2_sel,
                ex_i_control_reg_data_sel,
                ex_i_control_alu_mem_sel,
                ex_i_control_pc_rs1_sel,
                ex_i_control_busdata_rs2_rs1_sel,
                ex_i_control_memdata_ctl
            }
        )
    );

    pipeline_register #(.WIDTH(32+32+1+4+5+1+32+32+32+1+1+1+1+1+1+3)) ex_mem (
        .clk(clk),
        .rst(rst),
        .flush(branch_result),
        .stall(1'b0),
        .signal_in(
            {
                ex_i_program_counter_pc,
                ex_i_instr_mem_data,
                ex_exception,
                ex_exception_cause,
                ex_i_instr_decoder_rd,
                i_alu_branch,
                i_alu_result,
                busdata_rs2_rs1_mux_dout,
                ex_i_imm_gen_imm + pc_rs1_mux_dout,
                ex_i_control_reg_enable,
                ex_i_control_mem_enable,
                ex_i_control_branch_enable,
                ex_i_control_jump,
                ex_i_control_reg_data_sel,
                ex_i_control_alu_mem_sel,
                ex_i_control_memdata_ctl
            }
        ),
        .signal_out(
            {
                mem_i_program_counter_pc,
                mem_i_instr_mem_data,
                mem_exception,
                mem_exception_cause,
                mem_i_instr_decoder_rd,
                mem_i_alu_branch,
                mem_i_alu_result,
                mem_i_busdata_rs2_rs1_mux_dout,
                mem_i_pc_imm_sum,
                mem_i_control_reg_enable,
                mem_i_control_mem_enable,
                mem_i_control_branch_enable,
                mem_i_control_jump,
                mem_i_control_reg_data_sel,
                mem_i_control_alu_mem_sel,
                mem_i_control_memdata_ctl
            }
        )
    );

    pipeline_register #(.WIDTH(32+5+32+1+1)) mem_wb (
        .clk(clk),
        .rst(rst),
        .flush(1'b0),
        .stall(1'b0),
        .signal_in(
            {
                mem_i_program_counter_pc + 4,
                mem_i_instr_decoder_rd,
                alu_mem_mux_dout,
                mem_i_control_reg_enable,
                mem_i_control_reg_data_sel
            }
        ),
        .signal_out(
            {
                wb_i_pc_add_4,
                wb_i_instr_decoder_rd,
                wb_alu_mem_mux_dout,
                wb_i_control_reg_enable,
                wb_i_control_reg_data_sel
            }
        )
    );

	data_mem i_data_mem(
		.clk(clk),
		.we(bus_we),
        .memdata_ctl(bus_ctl),
		.addr(bus_addr),
		.wdata(bus_wdata),
		.rdata(i_data_mem_rdata),

        .instr_addr(i_program_counter_pc),
        .instr_data(i_instr_mem_data)
	);

    internal_bus_controller i_internal_bus_controller (
        .bus_addr(mem_i_alu_result),
        .io_rdata(bus_rdata),
        .csr_rdata(i_trap_csr_control_bus_rdata),
        .mem_rdata(i_data_mem_rdata),
        .rdata(i_internal_bus_controller_rdata)
    );

	alu i_alu(
		.number1(alu_src1_mux_dout),
		.number2(alu_src2_mux_dout),
		.mode(ex_i_control_alu_mode),
		.result(i_alu_result),
		.branch(i_alu_branch)
	);

	control i_control(
		.opcode(i_instr_decoder_opcode),
		.funct3(i_instr_decoder_funct3),
		.funct7(i_instr_decoder_funct7),
        .if_exception(id_exception),
        .if_exception_cause(id_exception_cause),
		.reg_enable(i_control_reg_enable),
		.mem_enable(i_control_mem_enable),
		.imm_type(i_control_imm_type),
		.alu_mode(i_control_alu_mode),
		.branch_enable(i_control_branch_enable),
		.jump(i_control_jump),
		.alu_src1_sel(i_control_alu_src1_sel),
		.alu_src2_sel(i_control_alu_src2_sel),
		.reg_data_sel(i_control_reg_data_sel),
		.alu_mem_sel(i_control_alu_mem_sel),
        .pc_rs1_sel(i_control_pc_rs1_sel),
        .busdata_rs2_rs1_sel(i_control_busdata_rs2_rs1_sel),
        .memdata_ctl(i_control_memdata_ctl),
        .exception(i_control_exception),
        .exception_cause(i_control_exception_cause)
	);

	imm_gen i_imm_gen(
		.instr(i_instr_decoder_immsrc),
		.imm_type(i_control_imm_type),
		.imm(i_imm_gen_imm)
	);

	instr_decoder i_instr_decoder(
		.instr(id_i_instr_mem_data),
		.opcode(i_instr_decoder_opcode),
		.rd(i_instr_decoder_rd),
		.rs1(i_instr_decoder_rs1),
		.rs2(i_instr_decoder_rs2),
		.funct3(i_instr_decoder_funct3),
		.funct7(i_instr_decoder_funct7),
		.immsrc(i_instr_decoder_immsrc)
	);

	register_file i_register_file(
		.clk(clk),
		.rs1(i_instr_decoder_rs1),
		.rs2(i_instr_decoder_rs2),
		.rdbg(5'b0),
		.rd(wb_i_instr_decoder_rd),
		.we(wb_i_control_reg_enable),
		.wdata(reg_data_mux_dout),
		.rdata1(i_register_file_rdata1),
		.rdata2(i_register_file_rdata2),
		.rdatadbg(i_register_file_rdatadbg)
	);

	program_counter i_program_counter(
		.clk(clk),
		.rst(rst),
		.stall(i_data_hazard_loaduse_happen),
		.branch(branch_result),
		.pc_new(i_trap_csr_control_jump ? i_trap_csr_control_target_pc : mem_i_pc_imm_sum),
		.pc(i_program_counter_pc)
	);

    mux2 alu_src1_mux (
        .data0(rs1_forwarding_mux_dout),
        .data1(ex_i_program_counter_pc),
        .sel(ex_i_control_alu_src1_sel),
        .dout(alu_src1_mux_dout)
    );

    mux2 alu_src2_mux (
        .data0(rs2_forwarding_mux_dout),
        .data1(ex_i_imm_gen_imm),
        .sel(ex_i_control_alu_src2_sel),
        .dout(alu_src2_mux_dout)
    );

    mux2 alu_mem_mux (
        .data0(mem_i_alu_result),
        .data1(i_internal_bus_controller_rdata),
        .sel(mem_i_control_alu_mem_sel),
        .dout(alu_mem_mux_dout)
    );

    mux2 reg_data_mux (
        .data0(wb_alu_mem_mux_dout),
        .data1(wb_i_pc_add_4),
        .sel(wb_i_control_reg_data_sel),
        .dout(reg_data_mux_dout)
    );

    mux2 pc_rs1_mux (
        .data0(ex_i_program_counter_pc),
        .data1(rs1_forwarding_mux_dout),
        .sel(ex_i_control_pc_rs1_sel),
        .dout(pc_rs1_mux_dout)
    );

    mux2 busdata_rs2_rs1_mux (
        .data0(rs2_forwarding_mux_dout),
        .data1(rs1_forwarding_mux_dout),
        .sel(ex_i_control_busdata_rs2_rs1_sel),
        .dout(busdata_rs2_rs1_mux_dout)
    );

	mux3 rs1_forwarding_mux (
		.data0(ex_i_register_file_rdata1),
		.data1(mem_i_alu_result),
		.data2(wb_alu_mem_mux_dout),
		.sel(ex_i_data_hazard_forwarding_rs1_sel),
		.dout(rs1_forwarding_mux_dout)
	);
	mux3 rs2_forwarding_mux (
		.data0(ex_i_register_file_rdata2),
		.data1(mem_i_alu_result),
		.data2(wb_alu_mem_mux_dout),
		.sel(ex_i_data_hazard_forwarding_rs2_sel),
		.dout(rs2_forwarding_mux_dout)
	);


	data_hazard i_data_hazard(
        .clk(clk),
		.rs1(i_instr_decoder_rs1),
		.rs2(i_instr_decoder_rs2),
		.ex_rd(ex_i_instr_decoder_rd),
		.mem_rd(mem_i_instr_decoder_rd),
		.ex_reg_we(ex_i_control_reg_enable),
		.mem_reg_we(mem_i_control_reg_enable),
		.ex_reg_data_sel(ex_i_control_reg_data_sel),
		.ex_alu_mem_sel(ex_i_control_alu_mem_sel),
		.loaduse_happen(i_data_hazard_loaduse_happen),
		.forwarding_rs1_sel(i_data_hazard_forwarding_rs1_sel),
		.forwarding_rs2_sel(i_data_hazard_forwarding_rs2_sel)
	);

	trap_csr_control i_trap_csr_control(
		.clk(clk),
		.rst(rst),
		.exception(mem_exception),
		.exception_cause(mem_exception_cause),
		.pc(mem_i_program_counter_pc),
		.mtval(mem_i_instr_mem_data),
		.timer_interrupt(0),
		.external_interrupt(0),
		.software_interrupt(0),
		.jump(i_trap_csr_control_jump),
		.target_pc(i_trap_csr_control_target_pc),
		.bus_addr(bus_addr),
		.bus_wdata(bus_wdata),
		.bus_ctl(bus_ctl),
		.bus_we(bus_we),
		.bus_rdata(i_trap_csr_control_bus_rdata)
	);


    assign branch_result = i_trap_csr_control_jump 
                        || mem_i_control_jump 
                        || (mem_i_alu_branch && mem_i_control_branch_enable);

    assign bus_addr = mem_i_alu_result;
    assign bus_wdata =  mem_i_busdata_rs2_rs1_mux_dout;
    assign bus_we = mem_i_control_mem_enable & ~i_trap_csr_control_jump;
    assign bus_ctl = mem_i_control_memdata_ctl;

    assign if_exception = 0;
    assign if_exception_cause = 0; // TODO
endmodule