module register_file (
    input clk,

    input [4:0] rs1,
    input [4:0] rs2,
    input [4:0] rdbg,

    input [4:0] rd,
    input we,
    input [31:0] wdata,

    output reg [31:0] rdata1,
    output reg [31:0] rdata2,
    output reg [31:0] rdatadbg
);
    reg [31:0] registers[0:31];

    always @ (*) begin
        rdata1 = rs1 == 0 ? 0 : registers[rs1];
        rdata2 = rs2 == 0 ? 0 : registers[rs2];
        rdatadbg = rdbg == 0 ? 0 : registers[rdbg];
        if (we && rd != 0) begin
            if (rs1 == rd)
                rdata1 = wdata;
            else if (rs2 == rd)
                rdata2 = wdata;
            else if (rdbg == rd)
                rdatadbg = wdata;
        end
    end

    always @ (posedge clk) begin
        if (we)
            registers[rd] <= wdata;
    end

endmodule