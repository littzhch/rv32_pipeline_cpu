module pipeline_register #(WIDTH = 32) (
    input clk,
    input rst,
    input flush,
    input stall,

    input [WIDTH-1:0] signal_in,
    output reg [WIDTH-1:0] signal_out
);
    reg stalled;
    always @ (posedge clk, posedge rst) begin
        if (rst) begin
            signal_out <= 0;
            stalled <= 0;
        end
        else if (flush) begin // 确保flush的优先级大于stall
            signal_out <= 0;
            stalled <= 0;
        end
        else if (stall) begin
            if (stalled) begin
                signal_out <= signal_in;
                stalled <= 0;
            end
            else
                stalled <= 1;
        end
        else begin
            signal_out <= signal_in;
            stalled <= 0;
        end
    end
endmodule