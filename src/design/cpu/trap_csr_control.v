
`define mstatus     0
`define mie         1
`define mtvec       2
`define mscratch    3
`define mepc        4
`define mcause      5
`define mtval       6
`define mip         7

`define mstatus_addr     32'h300
`define mie_addr         32'h304
`define mtvec_addr       32'h305
`define mscratch_addr    32'h340
`define mepc_addr        32'h341
`define mcause_addr      32'h342
`define mtval_addr       32'h343
`define mip_addr         32'h344


module trap_csr_control #(csr_base_addr = 32'hfffe_0000) (
    input clk,
    input rst,

    input exception,
    input [3:0] exception_cause,
    input [31:0] pc,
    input [31:0] mtval,
    input timer_interrupt,
    input external_interrupt,
    input software_interrupt,

    output reg jump,
    output reg [31:0] target_pc,

    // bus interface
    input [31:0] bus_addr,
    input [31:0] bus_wdata,
    input [2:0] bus_ctl,
    input bus_we,
    output reg [31:0] bus_rdata
);
    reg [31:0] trap_csrs[0:7];

    always @ (posedge clk, posedge rst) begin
        if (rst) begin
            trap_csrs[`mip] <= 0;
        end
        else begin
            trap_csrs[`mip][11] <= external_interrupt;
            trap_csrs[`mip][3] <= software_interrupt;
            trap_csrs[`mip][7] <= timer_interrupt;
        end
    end

    // 驱动 jump 和 target_pc
    always @ (*) begin
        jump = 0;
        target_pc = 0;
        if (exception) begin
            if (exception_cause != 10) begin
                jump = 1;
                target_pc = trap_csrs[`mtvec];
            end
            else begin // mret
                jump = 1;
                target_pc = trap_csrs[`mepc];
            end
        end
        else if (trap_csrs[`mstatus][3]
                && trap_csrs[`mie][11]
                && trap_csrs[`mip][11]) begin // external interrupt
                jump = 1;
                target_pc = trap_csrs[`mtvec];

        end
        else if (trap_csrs[`mstatus][3] 
                && trap_csrs[`mie][3] 
                && trap_csrs[`mip][3]) begin // software interrupt
                jump = 1;
                target_pc = trap_csrs[`mtvec];
        end
        else if (trap_csrs[`mstatus][3] 
                && trap_csrs[`mie][7] 
                && trap_csrs[`mip][7]) begin // timer interrupt
                jump = 1;
                target_pc = trap_csrs[`mtvec];
        end
    end

    // 驱动 bus_rdata
    always @ (*) begin
        case (bus_addr - csr_base_addr)
            `mstatus_addr:  bus_rdata = trap_csrs[`mstatus];
            `mie_addr:      bus_rdata = trap_csrs[`mie];
            `mtvec_addr:    bus_rdata = trap_csrs[`mtvec];
            `mscratch_addr: bus_rdata = trap_csrs[`mscratch];
            `mepc_addr:     bus_rdata = trap_csrs[`mepc];
            `mcause_addr:   bus_rdata = trap_csrs[`mcause];
            `mtval_addr:    bus_rdata = trap_csrs[`mtval];
            `mip_addr:      bus_rdata = trap_csrs[`mip];
            default:        bus_rdata = 0;
        endcase
    end

    // 更改寄存器
    // 优先级：exception > external interrupt > software interrupt > timer interrupt > csr rw
    always @ (posedge clk, posedge rst) begin
        if (rst) begin
            trap_csrs[`mstatus] <= {32'b1_1000_0000_0000};
            trap_csrs[`mie] <= 0;
            trap_csrs[`mtvec] <= 0;
            trap_csrs[`mscratch] <= 0;
            trap_csrs[`mepc] <= 0;
            trap_csrs[`mcause] <= 0;
            trap_csrs[`mtval] <= 0;
        end
        else if (exception) begin
            if (exception_cause != 10) begin
                trap_csrs[`mcause] <= {28'b0, exception_cause};
                trap_csrs[`mepc] <= pc;
                trap_csrs[`mtval] <= mtval;
                trap_csrs[`mstatus][7] <= trap_csrs[`mstatus][3];
                trap_csrs[`mstatus][3] <= 0;
            end
            else begin // mret
                trap_csrs[`mstatus][3] <= trap_csrs[`mstatus][7];
                trap_csrs[`mstatus][7] <= 1;
            end
        end
        else if (trap_csrs[`mstatus][3] 
                && trap_csrs[`mie][11] 
                && trap_csrs[`mip][11]) begin // external interrupt
                trap_csrs[`mcause] <= {1'b1, 31'd11};
                trap_csrs[`mepc] <= pc;
                trap_csrs[`mtval] <= mtval;
                trap_csrs[`mstatus][7] <= trap_csrs[`mstatus][3];
                trap_csrs[`mstatus][3] <= 0;
        end
        else if (trap_csrs[`mstatus][3] 
                && trap_csrs[`mie][3] 
                && trap_csrs[`mip][3]) begin // software interrupt
                trap_csrs[`mcause] <= {1'b1, 31'd3};
                trap_csrs[`mepc] <= pc;
                trap_csrs[`mtval] <= mtval;
                trap_csrs[`mstatus][7] <= trap_csrs[`mstatus][3];
                trap_csrs[`mstatus][3] <= 0;
        end
        else if (trap_csrs[`mstatus][3] 
                && trap_csrs[`mie][7] 
                && trap_csrs[`mip][7]) begin // timer interrupt
                trap_csrs[`mcause] <= {1'b1, 31'd7};
                trap_csrs[`mepc] <= pc;
                trap_csrs[`mtval] <= mtval;
                trap_csrs[`mstatus][7] <= trap_csrs[`mstatus][3];
                trap_csrs[`mstatus][3] <= 0;
        end
        else if (bus_we) begin
            case (bus_addr - csr_base_addr)
                `mstatus_addr: begin
                    if (bus_ctl == 3'b001)
                        trap_csrs[`mstatus] <= bus_wdata;
                    else if (bus_ctl == 3'b010)
                        trap_csrs[`mstatus] <= bus_wdata | trap_csrs[`mstatus];
                    else if (bus_ctl == 3'b011)
                        trap_csrs[`mstatus] <= bus_wdata & trap_csrs[`mstatus];
                end
                `mie_addr: begin
                    if (bus_ctl == 3'b001)
                        trap_csrs[`mie] <= bus_wdata;
                    else if (bus_ctl == 3'b010)
                        trap_csrs[`mie] <= bus_wdata | trap_csrs[`mie];
                    else if (bus_ctl == 3'b011)
                        trap_csrs[`mie] <= bus_wdata & trap_csrs[`mie];
                end
                `mtvec_addr: begin
                    if (bus_ctl == 3'b001)
                        trap_csrs[`mtvec] <= bus_wdata;
                    else if (bus_ctl == 3'b010)
                        trap_csrs[`mtvec] <= bus_wdata | trap_csrs[`mtvec];
                    else if (bus_ctl == 3'b011)
                        trap_csrs[`mtvec] <= bus_wdata & trap_csrs[`mtvec];
                end
                `mscratch_addr: begin
                    if (bus_ctl == 3'b001)
                        trap_csrs[`mscratch] <= bus_wdata;
                    else if (bus_ctl == 3'b010)
                        trap_csrs[`mscratch] <= bus_wdata | trap_csrs[`mscratch];
                    else if (bus_ctl == 3'b011)
                        trap_csrs[`mscratch] <= bus_wdata & trap_csrs[`mscratch];
                end
                `mepc_addr: begin
                    if (bus_ctl == 3'b001)
                        trap_csrs[`mepc] <= bus_wdata;
                    else if (bus_ctl == 3'b010)
                        trap_csrs[`mepc] <= bus_wdata | trap_csrs[`mepc];
                    else if (bus_ctl == 3'b011)
                        trap_csrs[`mepc] <= bus_wdata & trap_csrs[`mepc];
                end
                `mcause_addr: begin
                    if (bus_ctl == 3'b001)
                        trap_csrs[`mcause] <= bus_wdata;
                    else if (bus_ctl == 3'b010)
                        trap_csrs[`mcause] <= bus_wdata | trap_csrs[`mcause];
                    else if (bus_ctl == 3'b011)
                        trap_csrs[`mcause] <= bus_wdata & trap_csrs[`mcause];
                end
                `mtval_addr: begin
                    if (bus_ctl == 3'b001)
                        trap_csrs[`mtval] <= bus_wdata;
                    else if (bus_ctl == 3'b010)
                        trap_csrs[`mtval] <= bus_wdata | trap_csrs[`mtval];
                    else if (bus_ctl == 3'b011)
                        trap_csrs[`mtval] <= bus_wdata & trap_csrs[`mtval];
                end
            endcase
        end
    end
endmodule