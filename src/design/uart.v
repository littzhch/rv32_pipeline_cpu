// uart 控制寄存器
// addr     name                prot    description
// base+0   uart_send_data      W          
// base+4   uart_send_ready     R
// base+8   uart_read_next      W
// base+12  uart_read_data      R
// base+16  uart_read_ready     R

module uart #(base = 32'hffff_ffec, bps = 1500_000) (
    input clk,
    input rst,

    // bus interface
    input [31:0] bus_addr,
    input [31:0] bus_wdata,
    input bus_we,
    output reg [31:0] bus_rdata,

    // uart port
    output uart_rx,
    input uart_tx
);

    wire [7:0] uart_send_data;
    reg uart_send_enable;
    wire uart_send_ready;
    reg uart_read_next;
    wire [7:0] uart_read_data;
    wire uart_read_ready;

    assign uart_send_data = bus_wdata[7:0];

    always @ (*) begin
        uart_send_enable = 0;
        uart_read_next = 0;
        bus_rdata = 0;
        case (bus_addr - base)
            0:
                if (bus_we)
                    uart_send_enable = 1;
            4:  bus_rdata = uart_send_ready;
            8:
                if (bus_we)
                    uart_read_next = 1;
            12: bus_rdata = {{24{uart_read_data[7]}}, uart_read_data};
            16: bus_rdata = uart_read_ready;
        endcase
    end

    uart_read #(bps) i_uart_read  (
        .clk(clk),
        .rst(rst), 
        .uart_tx(uart_tx),
        .next(uart_read_next),
        .not_empty(uart_read_ready),
        .data(uart_read_data)
    );

    uart_send #(bps) i_uart_send (
        .clk(clk),
        .rst(rst),
        .enable(uart_send_enable),
        .data(uart_send_data),
        .uart_rx(uart_rx),
        .ready(uart_send_ready)
    );
endmodule


module uart_read #(BPS = 1500_000) (
    input clk, // 100MHz
    input rst, 
    input uart_tx,
    input next,
    output not_empty,
    output [7:0] data
);
    parameter LENGTH = 100_000_000 / BPS;
    parameter BUFFER_SIZE_BYTE = 256;

    reg read_signal, read_end;
    reg [31:0] read_signal_count;

    initial begin
        read_signal = 0;
        read_end = 0;
        read_signal_count = 0;
    end

    always @ (posedge clk) begin
        if (rst || read_end) begin
            read_signal <= 0;
            read_signal_count <= 0;
        end
        else begin
            if (read_signal == 0) begin
                if (uart_tx == 0) begin
                    read_signal_count <= read_signal_count + 1;
                    if (read_signal_count > LENGTH / 2) begin
                        read_signal <= 1;
                        read_signal_count <= 0;
                    end
                end
            end
        end
    end

    reg [BUFFER_SIZE_BYTE * 8 - 1:0] buffer;
    reg [31:0] writeptr;
    reg [31:0] readptr;
    wire [31:0] next_writeptr;
    wire [31:0] next_readptr;

    initial begin
        buffer = 0;
        writeptr = 0;
        readptr = 0;
    end

    assign next_writeptr = writeptr == (BUFFER_SIZE_BYTE * 8 - 1) ? 0 : writeptr + 1;
    assign next_readptr = readptr == (BUFFER_SIZE_BYTE * 8 - 8) ? 0 : readptr + 8;
    
    reg [31:0] read_count;
    reg [7:0] bit_count;
    initial begin
        read_count = 0;
        bit_count = 0;
    end

    always @ (posedge clk) begin
        if (rst || read_signal == 0) begin
            if (rst) begin
                writeptr <= 0;
                buffer = 0;
            end
            read_count <= 0;
            bit_count <= 0;
            read_end <= 0;
        end
        else if (read_signal) begin
            read_count <= read_count + 1;
            if (read_count >= LENGTH) begin
                read_count <= 0;
                if (bit_count <= 7) begin
                    bit_count <= bit_count + 1;
                    buffer[writeptr] <= uart_tx;
                    writeptr <= next_writeptr;
                end
                else begin
                    bit_count <= 0;
                    read_end <= 1;
                end
            end
        end
    end

    
    assign not_empty = (writeptr < readptr) || (writeptr - readptr >= 8);
    assign data = {buffer[readptr + 7], 
                    buffer[readptr + 6],
                    buffer[readptr + 5],
                    buffer[readptr + 4],
                    buffer[readptr + 3],
                    buffer[readptr + 2],
                    buffer[readptr + 1],
                    buffer[readptr]};

    always @ (posedge clk) begin
        if (rst) begin
            readptr <= 0;
        end
        else if (not_empty) begin
            if (next)
                readptr <= next_readptr;
        end
    end

endmodule

module uart_send #(BPS = 1500_000) (
    input clk,
    input rst,
    input enable,
    input [7:0] data,
    output reg uart_rx,
    output ready
);
    parameter LENGTH  = 100_000_000 / BPS;

    reg [9:0] to_be_send;
    reg is_working;
    reg work_end;

    always @ (posedge clk) begin
        if (rst || work_end)
            is_working <= 0;
        else if (enable && is_working == 0)
            is_working <= 1;
    end
    assign ready = ! is_working;

    always @(posedge clk) begin
        if (rst)
            to_be_send <= 0;
        else if (enable && is_working == 0) begin
            to_be_send <= {1'b1, data, 1'b0};
        end
    end

    reg [3:0] bit_count;
    reg [31:0] period_count;
    always @ (posedge clk) begin
        if (rst || ! is_working) begin
            bit_count <= 0;
            work_end <= 0;
            period_count <= 0;
            uart_rx <= 1;
        end
        else begin
            uart_rx <= to_be_send[bit_count];
            if (period_count >= LENGTH) begin
                period_count <= 0;
                if (bit_count == 9) begin
                    work_end <= 1;
                end
                else begin
                    bit_count <= bit_count + 1;
                end
            end
            else
                period_count <= period_count + 1;
        end
    end
endmodule

