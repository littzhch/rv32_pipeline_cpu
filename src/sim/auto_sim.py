import re
import os
import codecs
import copy
import sys
from abc import ABC, abstractmethod
import tomlkit as toml


def remove_comment(code: str) -> str:
    re_str = r"(?://.*)|(?:/\*[\S\s]*?\*/)"
    return re.sub(re_str, "", code)


class Variable:
    def __init__(self):
        self.type = ""
        self.width_from = ""
        self.width_to = ""
        self.name = ""

    def declaration_str(self) -> str:
        initialization = " = 0" if self.type == "reg" else ""
        if self.width_from == '0' and self.width_to == '0':
            return f"{self.type} {self.name}{initialization};\n"
        else:
            return f"{self.type} [{self.width_from}:{self.width_to}] {self.name}{initialization};\n"


class Arguments:
    def __init__(self, arg_str: str):
        self.arg_type = 0
        self.arg_str = arg_str
        self.args = []

        if arg_str == "":
            return

        clean_str = arg_str.replace('#', '').strip()[1:-1]
        if '.' in clean_str:
            self.arg_type = 1
            pair_strs = [pair_str.split('(') for pair_str in clean_str.replace(')', '').replace('.', '').split(',')]
            self.args = {key.strip(): val.strip() for [key, val] in pair_strs}
        else:
            self.arg_type = 0
            self.args = [arg.strip() for arg in clean_str.split(',')]


class Parameters:
    """模块定义时指定的参数"""
    params_pattern = r"#\(([\S\s]*?)\)"

    def __init__(self, declaration: str):
        self.params: dict
        match_obj = re.search(self.params_pattern, declaration)
        if match_obj is None:
            self.params = {}
        else:
            params_pairs = match_obj.group(1).replace("parameter", '').split(',')
            self.params = {key.strip(): val.strip() for [key, val] in [pair.split('=') for pair in params_pairs]}

    def assign(self, args: Arguments):
        if args.arg_type == 0:
            i = 0
            max_i = len(args.args)
            for key in self.params.keys():
                if i == max_i:
                    break
                self.params[key] = args.args[i]
                i += 1
        elif args.arg_type == 1:
            self.params.update(args.args)


class Port:
    def __init__(self, declaration: str):
        self.type = ""
        self.width_from = ""
        self.width_to = ""
        self.name = ""

        declaration = declaration.replace(" reg ", " ")\
                                 .replace(" wire ", " ")\
                                 .replace(" reg[", " [") \
                                 .replace(" wire[", " [")\
                                 .replace(" signed ", " ")\
                                 .replace(" signed[", " [")
        re_str = r"\s*([a-zA-Z_][0-9a-zA-Z_]*)(?:(?:\s+)|(?:\s*\[([\S\s]+?):([\S\s]+?)\]\s*))"\
                 r"([a-zA-Z_][0-9a-zA-Z_]*)"
        m = re.match(re_str, declaration)
        if m is not None:
            match_list = m.groups()
            self.type = match_list[0]
            self.width_from = match_list[1] if match_list[1] is not None else "0"
            self.width_to = match_list[2] if match_list[2] is not None else "0"
            self.name = match_list[3]
        else:
            raise RuntimeError("\"" + declaration + "\" 不是有效的端口定义")

    def assign(self, params: Parameters):
        for (key, val) in params.params.items():
            self.width_from = self.width_from.replace(key, val)
            self.width_to = self.width_to.replace(key, val)
        try:
            self.width_from = str(eval(self.width_from, {}))
        except NameError:
            pass
        try:
            self.width_to = str(eval(self.width_to, {}))
        except NameError:
            pass

    def get_variable(self) -> Variable:
        var = Variable()
        var.type = "wire" if self.type == "output" else "reg"
        var.width_from = self.width_from
        var.width_to = self.width_to
        var.name = self.name
        return var


class ModuleDeclaration:
    """模块定义"""
    declaration_pattern = r"module\s+[\S\s]+?\)\s*;"
    name_pattern = r"module\s*([a-zA-Z_][a-zA-Z0-9_]*)"
    ports_pattern = r"(?:#\s*\((?:[\s\S]*?)\)|)\s*\(([\S\s]*?)\)\s*;"

    @classmethod
    def find_module_str(cls,  module_name: str, code: str):
        code = remove_comment(code)
        decls = re.findall(cls.declaration_pattern, code)
        for decl in decls:
            if re.match(rf"module\s*{module_name}", decl) is not None:
                return decl
        return None

    def __init__(self, declaration: str):
        self.parameters = Parameters(declaration)
        self.module_name = ""
        self.ports = []
        self.clock_name = ""
        self.reset_name = ""

        match_obj = re.search(self.name_pattern, declaration)
        if match_obj is None:
            raise RuntimeError(f"{declaration} 中不包含有效的模块名")
        self.module_name = match_obj.group(1)

        match_obj = re.search(self.ports_pattern, declaration)
        if match_obj is None:
            raise RuntimeError(f"{declaration} 中不包含有效的端口定义")
        port_strs = match_obj.group(1).split(',')
        for port_str in port_strs:
            self.ports.append(Port(port_str))

        for port in self.ports:
            if "clk" in port.name.lower() or "clock" in port.name.lower():
                self.clock_name = port.name
                continue
            if "rst" in port.name.lower() or "reset" in port.name.lower():
                self.reset_name = port.name


class ModuleInstance:
    def __init__(self, module: ModuleDeclaration, instance_name: str, args: Arguments):
        self.module_name = module.module_name
        self.instance_name = instance_name
        self.parameters = copy.deepcopy(module.parameters)
        self.parameters.assign(args)
        self.arg_str = args.arg_str
        self.ports = copy.deepcopy(module.ports)
        for port in self.ports:
            port.assign(self.parameters)
        self.clock_name = module.clock_name
        self.reset_name = module.reset_name

    def instance_str(self):
        result = f"{self.module_name} {self.arg_str} {self.instance_name} (\n"
        for port in self.ports[:-1]:
            result += f"\t.{port.name}({port.name}),\n"
        result += f"\t.{self.ports[-1].name}({self.ports[-1].name})\n);\n"
        return result

    def var_str(self):
        result = ""
        for port in self.ports:
            var = port.get_variable()
            result += var.declaration_str()
        return result


class SimTask(ABC):
    def __init__(self, start_tick, total_ticks):
        self.start_tick = start_tick
        self.total_ticks = total_ticks

    @abstractmethod
    def generate_str(self) -> str:
        pass

    def get_end_tick(self) -> int:
        return self.start_tick + self.total_ticks

    def set_start_tick(self, start_tick: int):
        self.start_tick = start_tick

    def get_start_tick(self):
        return self.start_tick


class CombinationTask(SimTask):
    def __init__(self, start_tick: int, var_info: dict):
        total_ticks = 1
        for val_list in var_info.values():
            total_ticks *= len(val_list)
        super().__init__(start_tick, total_ticks)
        self.vars = var_info

    def generate_str(self) -> str:
        result = "\ninitial begin\n"
        if self.start_tick > 0:
            result += f"\t#({self.start_tick}*PERIOD);\n"

        var_list = list(self.vars.keys())
        ind_max = len(var_list)
        var_ind = 0

        def gen_combination():
            nonlocal result
            nonlocal var_ind
            if var_ind == ind_max:
                result += f"\t#(PERIOD);\n"
                var_ind -= 1
                return
            for val in self.vars[var_list[var_ind]]:
                result += f"\t{var_list[var_ind]} = {val};\n"
                var_ind += 1
                gen_combination()
            var_ind -= 1

        gen_combination()

        for var in var_list:
            result += f"\t{var} = 0;\n"
        result += "end\n"
        return result


class SequenceTask(SimTask):
    def __init__(self, start_tick: int, vars_info: dict):
        total_ticks = max([len(var) for var in vars_info.values()])
        super().__init__(start_tick, total_ticks)
        self.vars = vars_info

    def generate_str(self) -> str:
        result = "\ninitial begin\n"
        if self.start_tick > 0:
            result += f"\t#({self.start_tick}*PERIOD);\n"

        for ind in range(0, self.total_ticks):
            for (key, val) in self.vars.items():
                if ind < len(val):
                    result += f"\t{key} = {val[ind]};\n"
            result += f"\t#(PERIOD);\n"

        for key in self.vars.keys():
            result += f"\t{key} = 0;\n"

        result += "end\n"
        return result


def verilog_src_files(root: str):
    for (dir_path, dir_names, file_names) in os.walk(root):
        for file_name in file_names:
            if file_name[-2:] == ".v":
                yield os.path.join(dir_path, file_name)


def generate_simulation(config: dict) -> str:
    module = None
    for file_path in verilog_src_files(config['search_path']):
        f = codecs.open(file_path, "r", encoding="UTF-8")
        code = f.read()
        f.close()
        module_str = ModuleDeclaration.find_module_str(config["module"], code)
        if module_str is not None:
            module = ModuleDeclaration(module_str)
            break
    if module is None:
        raise RuntimeError(f"{config['module']}模块定义未找到")

    inst = ModuleInstance(module, f"test_{module.module_name}", Arguments(config['args']))

    task_start_tick = 0
    result = f"module tb_{module.module_name};\n"
    result += f"parameter PERIOD = {config['period']};\n\n"
    result += inst.var_str()
    result += '\n'
    result += inst.instance_str()

    if inst.clock_name != "":
        task_start_tick = 1
        result += f"\ninitial begin\n"
        if config['clock_edge'] == "posedge":
            result += "\t#(PERIOD/2);\n"
        elif config['clock_edge'] == "negedge":
            pass
        else:
            raise RuntimeError(f"{config['clock_edge']} 不是有效的 clock_edge 值")
        result += f"\tforever #(PERIOD/2) {inst.clock_name} = ~{inst.clock_name};\nend\n"

    if inst.reset_name != "":
        task_start_tick = 3
        result += f"\ninitial begin\n\t{inst.reset_name} = 1;\n"\
                  f"\t#(2*PERIOD) {inst.reset_name} = 0;\nend\n"

    tasks: list[SimTask] = []
    for info in config['sequence']:
        tasks.append(SequenceTask(task_start_tick, info))
    for info in config['combination']:
        tasks.append(CombinationTask(task_start_tick, info))

    for i in range(0, len(tasks) - 1):
        tasks[i + 1].set_start_tick(tasks[i].get_end_tick() + 1)

    for i in range(0, len(tasks)):
        result += tasks[i].generate_str()

    if tasks:
        result += f"\ninitial begin\n\t#({tasks[-1].get_end_tick()}*PERIOD) $finish;\nend\n\nendmodule\n"
    else:
        result += f"\ninitial begin\n\n\t$finish;\nend\n\nendmodule\n"
    return result


def process_config(config: dict):
    if "module" not in config.keys():
        raise RuntimeError("未指定模块名")

    empty_config = {
        "module": "",
        "args": "",
        "period": 20,
        "clock_edge": "posedge",
        "search_path": "./",
        "target_file": f"./{config['module']}_test.v",
        "combination": [],
        "sequence": []
    }

    unknown_keys = [key for key in config.keys() if key not in empty_config.keys()]
    for key in unknown_keys:
        print(f"警告：未知的键名 {key} 将被忽略")

    empty_config.update(config)
    config.update(empty_config)


def main():
    config_path = os.path.abspath(sys.argv[1])
    os.chdir(os.path.dirname(config_path))
    f = codecs.open(config_path, "r", encoding="UTF-8")
    config = dict(toml.load(f))
    f.close()

    process_config(config)

    if not os.path.exists(os.path.dirname(config['target_file'])):
        os.makedirs(os.path.dirname(config['target_file']))
    f = codecs.open(config['target_file'], "w", encoding="UTF-8")
    f.write(generate_simulation(config))
    f.close()


if __name__ == "__main__":
    main()
