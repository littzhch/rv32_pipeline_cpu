module tb_trap_csr_control;
parameter PERIOD = 20;

reg clk = 0;
reg rst = 0;
reg exception = 0;
reg [3:0] exception_cause = 0;
reg [31:0] pc = 0;
reg [31:0] mtval = 0;
reg timer_interrupt = 0;
reg external_interrupt = 0;
reg software_interrupt = 0;
wire jump;
wire target_pc;
reg [31:0] bus_addr = 0;
reg [31:0] bus_wdata = 0;
reg [2:0] bus_ctl = 0;
reg bus_we = 0;
wire [31:0] bus_rdata;

trap_csr_control  test_trap_csr_control (
	.clk(clk),
	.rst(rst),
	.exception(exception),
	.exception_cause(exception_cause),
	.pc(pc),
	.mtval(mtval),
	.timer_interrupt(timer_interrupt),
	.external_interrupt(external_interrupt),
	.software_interrupt(software_interrupt),
	.jump(jump),
	.target_pc(target_pc),
	.bus_addr(bus_addr),
	.bus_wdata(bus_wdata),
	.bus_ctl(bus_ctl),
	.bus_we(bus_we),
	.bus_rdata(bus_rdata)
);

initial begin
	#(PERIOD/2);
	forever #(PERIOD/2) clk = ~clk;
end

initial begin
	rst = 1;
	#(2*PERIOD) rst = 0;
end

initial begin
	#(3*PERIOD);
	bus_ctl = 3'b010;
	bus_addr = 32'hfffe_0300;
	bus_we = 1;
	bus_wdata = 4'b1000;
	#(PERIOD);
	bus_ctl = 0;
	bus_addr = 0;
	bus_we = 0;
	bus_wdata = 0;
end

initial begin
	#(5*PERIOD);
	external_interrupt = 0;
	pc = 32'h3024;
	#(PERIOD);
	external_interrupt = 1;
	pc = 32'h3028;
	#(PERIOD);
	pc = 32'h302c;
	#(PERIOD);
	pc = 32'h3030;
	#(PERIOD);
	pc = 32'h3034;
	#(PERIOD);
	external_interrupt = 0;
	pc = 0;
end

initial begin
	#(10*PERIOD) $finish;
end

endmodule
