module tb_top;
parameter PERIOD = 20;

reg clk = 0;
reg rst = 0;
wire uart_rx;
reg uart_tx = 0;

top  test_top (
	.clk(clk),
	.rst(rst),
	.uart_rx(uart_rx),
	.uart_tx(uart_tx)
);

initial begin
	#(PERIOD/2);
	forever #(PERIOD/2) clk = ~clk;
end

initial begin
	rst = 1;
	#(2*PERIOD) rst = 0;
end
endmodule
