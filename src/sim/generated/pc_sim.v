module tb_program_counter;
parameter PERIOD = 20;

reg clk = 0;
reg rst = 0;
reg stall = 0;
reg [1:0] inc_mode = 0;
reg [31:0] pc_inc = 0;
wire [31:0] pc;
wire [31:0] pc_add_4;

program_counter  test_program_counter (
	.clk(clk),
	.rst(rst),
	.stall(stall),
	.inc_mode(inc_mode),
	.pc_inc(pc_inc),
	.pc(pc),
	.pc_add_4(pc_add_4)
);

initial begin
	#(PERIOD/2);
	forever #(PERIOD/2) clk = ~clk;
end

initial begin
	rst = 1;
	#(2*PERIOD) rst = 0;
end

initial begin
	#(3*PERIOD+1);
	pc_inc = -8;
	inc_mode = 0;
	stall = 0;
	#(PERIOD);
	inc_mode = 0;
	stall = 0;
	#(PERIOD);
	inc_mode = 0;
	stall = 0;
	#(PERIOD);
	inc_mode = 0;
	stall = 0;
	#(PERIOD);
	inc_mode = 1;
	stall = 0;
	#(PERIOD);
	inc_mode = 0;
	stall = 0;
	#(PERIOD);
	stall = 0;
	#(PERIOD);
	stall = 0;
	#(PERIOD);
	stall = 1;
	#(PERIOD);
	stall = 1;
	#(PERIOD);
	stall = 1;
	#(PERIOD);
	stall = 1;
	#(PERIOD);
	stall = 1;
	#(PERIOD);
	stall = 1;
	#(PERIOD);
	pc_inc = 0;
	inc_mode = 0;
	stall = 0;
end

initial begin
	#(17*PERIOD) $finish;
end

endmodule
