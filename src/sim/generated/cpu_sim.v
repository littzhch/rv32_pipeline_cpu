module tb_cpu;
parameter PERIOD = 20;

reg clk = 0;
reg rst = 0;

cpu  test_cpu (
	.clk(clk),
	.rst(rst)
);

initial begin
	#(PERIOD/2);
	forever #(PERIOD/2) clk = ~clk;
end

initial begin
	rst = 1;
	#(2*PERIOD+1) rst = 0;
end
endmodule
