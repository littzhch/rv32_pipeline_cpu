module tb_alu;
parameter PERIOD = 20;

reg [31:0] number1 = 0;
reg [31:0] number2 = 0;
reg [3:0] mode = 0;
wire [31:0] result;
wire branch;

alu  test_alu (
	.number1(number1),
	.number2(number2),
	.mode(mode),
	.result(result),
	.branch(branch)
);

initial begin
	mode = 4'b0000;
	number1 = -132;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = -3;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = -2;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = -1;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 0;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 2;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 3;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 100;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	mode = 4'b1000;
	number1 = -132;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = -3;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = -2;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = -1;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 0;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 2;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 3;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 100;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	mode = 4'b0111;
	number1 = -132;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = -3;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = -2;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = -1;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 0;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 2;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 3;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 100;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	mode = 4'b0110;
	number1 = -132;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = -3;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = -2;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = -1;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 0;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 2;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 3;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 100;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	mode = 4'b0100;
	number1 = -132;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = -3;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = -2;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = -1;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 0;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 2;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 3;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 100;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	mode = 4'b0001;
	number1 = -132;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = -3;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = -2;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = -1;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 0;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 2;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 3;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 100;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	mode = 4'b0101;
	number1 = -132;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = -3;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = -2;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = -1;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 0;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 2;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 3;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 100;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	mode = 4'b1101;
	number1 = -132;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = -3;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = -2;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = -1;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 0;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 2;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 3;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 100;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	mode = 4'b0010;
	number1 = -132;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = -3;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = -2;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = -1;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 0;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 2;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 3;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 100;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	mode = 4'b0011;
	number1 = -132;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = -3;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = -2;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = -1;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 0;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 2;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 3;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	number1 = 100;
	number2 = -132;
	#(PERIOD);
	number2 = -3;
	#(PERIOD);
	number2 = -2;
	#(PERIOD);
	number2 = -1;
	#(PERIOD);
	number2 = 0;
	#(PERIOD);
	number2 = 2;
	#(PERIOD);
	number2 = 3;
	#(PERIOD);
	number2 = 100;
	#(PERIOD);
	mode = 0;
	number1 = 0;
	number2 = 0;
end

initial begin
	#(640*PERIOD) $finish;
end

endmodule
