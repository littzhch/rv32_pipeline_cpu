module tb_instr_mem;
parameter PERIOD = 20;

reg [31:0] addr = 0;
wire [31:0] data;

instr_mem  test_instr_mem (
	.addr(addr),
	.data(data)
);

initial begin
	addr = 32'h3004;
	#(PERIOD);
	addr = 32'h3008;
	#(PERIOD);
	addr = 32'h300c;
	#(PERIOD);
	addr = 32'h3010;
	#(PERIOD);
	addr = 32'h3014;
	#(PERIOD);
	addr = 32'h3018;
	#(PERIOD);
	addr = 0;
end

initial begin
	#(6*PERIOD) $finish;
end

endmodule
