module tb_register_file;
parameter PERIOD = 20;

reg clk = 0;
reg [4:0] rs1 = 0;
reg [4:0] rs2 = 0;
reg [4:0] rdbg = 0;
reg [4:0] rd = 0;
reg we = 0;
reg [31:0] wdata = 0;
wire [31:0] rdata1;
wire [31:0] rdata2;
wire [31:0] rdatadbg;

register_file  test_register_file (
	.clk(clk),
	.rs1(rs1),
	.rs2(rs2),
	.rdbg(rdbg),
	.rd(rd),
	.we(we),
	.wdata(wdata),
	.rdata1(rdata1),
	.rdata2(rdata2),
	.rdatadbg(rdatadbg)
);

initial begin
	#(PERIOD/2);
	forever #(PERIOD/2) clk = ~clk;
end

initial begin
	#(1*PERIOD);
	we = 1;
	rd = 0;
	wdata = 1;
	rs1 = 23;
	rs2 = 31;
	rdbg = 3;
	#(PERIOD);
	rd = 1;
	wdata = 2;
	#(PERIOD);
	rd = 3;
	wdata = 4;
	#(PERIOD);
	rd = 12;
	wdata = 13;
	#(PERIOD);
	rd = 23;
	wdata = 24;
	#(PERIOD);
	rd = 31;
	wdata = 32;
	#(PERIOD);
	we = 0;
	rd = 0;
	wdata = 0;
	rs1 = 0;
	rs2 = 0;
	rdbg = 0;
end

initial begin
	#(7*PERIOD) $finish;
end

endmodule
